# Translation of heimdal debconf templates to French
# Copyright (C) 2007 Christian Perrier <bubulle@debian.org>
# Copyright (C) 2004-2006 Rémi Pannequin <remi.pannequin@laposte.net>
# This file is distributed under the same license as the heimdal package.
#
# Christian Perrier <bubulle@debian.org>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: heimdal@packages.debian.org\n"
"POT-Creation-Date: 2011-08-04 16:42+0200\n"
"PO-Revision-Date: 2007-09-30 19:33+0200\n"
"Last-Translator: Christian Perrier <bubulle@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#. Type: string
#. Description
#: ../heimdal-kdc.templates:1001
msgid "Local realm name:"
msgstr "Nom de l'aire (« realm ») locale :"

#. Type: string
#. Description
#: ../heimdal-kdc.templates:1001
msgid "Please enter the name of the local Kerberos realm."
msgstr "Veuillez indiquer le nom de l'aire Kerberos locale."

#. Type: string
#. Description
#: ../heimdal-kdc.templates:1001
msgid ""
"Using the uppercase domain name is common. For instance, if the host name is "
"host.example.org, then the realm will become EXAMPLE.ORG. The default for "
"this host is ${default_realm}."
msgstr ""
"Le nom de l'aire (« realm ») est généralement le nom de domaine, en lettres "
"majuscules. Par exemple, si le nom d'hôte est « host.example.com », alors le "
"nom de l'aire sera « EXAMPLE.COM ». La valeur par défaut pour cet hôte est "
"« ${default_realm} »."

#. Type: password
#. Description
#: ../heimdal-kdc.templates:2001
msgid "KDC password:"
msgstr "Mot de passe de centre de distribution de clés (KDC) :"

#. Type: password
#. Description
#: ../heimdal-kdc.templates:2001
msgid ""
"Heimdal can encrypt the key distribution center (KDC) data with a password. "
"A hashed representation of this password will be stored in /var/lib/heimdal-"
"kdc/m-key."
msgstr ""
"Heimdal peut chiffrer les données du centre de distribution de clés (KDC : "
"« Key Distribution Center ») avec un mot de passe. Une représentation hachée "
"de ce mot de passe sera enregistrée dans « /var/lib/heimdal-kdc/m-key »."
